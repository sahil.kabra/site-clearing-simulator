import React from "react";
import "./App.css";
import { ProcessedCommand } from "./engine/core/Command";
import * as CostingEngine from "./engine/core/Costing";
import { Block, BlockWithType } from "./engine/core/Site";
import { Simulator } from "./engine/Simulator";
import { BlockType } from "./web/Block";
import { Costing } from "./web/Costing";
import { Site } from "./web/Site";
import { Aside } from "./web/Aside";

export type AppProps = {};
export type AppState = {
  command: string;
  costing?: CostingEngine.CostingResponse;
  errorMessage?: string;
  layout?: BlockType[][];
  simulator?: Simulator;
  simulationEnded: boolean;
};

class App extends React.Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);
    this.state = {
      command: "",
      costing: undefined,
      errorMessage: undefined,
      layout: undefined,
      simulator: undefined,
      simulationEnded: false,
    };
  }

  render() {
    const simulationClassName = "app-simulation " + (this.state.simulationEnded ? "blur" : "");
    return (
      <div className="app">
        <header className="app-header">Site clearing simulator</header>
        <div className="app-body">
          {!this.state.layout && (
            // Render the file uploader if there is no layout present.
            <div data-testid="fileInput" className="fileInput">
              <input type="file" onChange={(e) => this.readFile(e)} />
            </div>
          )}
          {this.state.layout && (
            // Render the site if a layout has been uploaded
            <div>
              <div className={simulationClassName}>
                <Site bulldozerDirection={this.state.simulator!.getBulldozerDirection()} layout={this.state.layout} />
                <div>
                  <div className="input">
                    <label>Enter Command</label>
                    <input
                      maxLength={2}
                      type="text"
                      value={this.state.command}
                      onChange={this.handleChange}
                      onKeyDown={this.handleKeyPress}
                    />
                  </div>
                  <div className="error-message">
                    {this.state.errorMessage && <label>{this.state.errorMessage}</label>}
                  </div>
                </div>
              </div>
            </div>
          )}
          {this.state.simulationEnded && (
            // Render the costing screen once the user has quit
            <div className="app-costing">
              <Costing {...this.state.costing!} />
            </div>
          )}
        </div>
        {this.state.layout && (
          // Render the aside if the simulation is in progress
          <div className={"app-aside " + (this.state.simulationEnded ? "blur" : "")}>
            <Aside />
          </div>
        )}
      </div>
    );
  }

  readFile(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();

    const reader = new FileReader();

    reader.onload = async (e) => {
      if (e.target?.result) {
        if (typeof e.target.result === "string") {
          const layout: string[] = e.target.result.split(/\r|\n/).slice(0, -1);
          this.setState({
            layout: layout.map((r) => [...r] as BlockType[]),
            simulator: new Simulator(layout),
          });
        }
      }
    };

    e.target.files && reader.readAsText(e.target.files[0]);
  }

  // update the command in the state when the user enters a character in the input
  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;
    this.setState({ errorMessage: undefined, command: newValue });
  };

  // process the command when the user presses enter
  handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter" && this.state.simulator) {
      try {
        this.state.simulator.processCommand(this.state.command);
        this.setState({
          command: "",
          layout: this.processLayout(this.state.simulator.getLayout(), this.state.simulator.getBulldozerBlock()),
          simulationEnded: this.state.simulator.isEnded(),
          costing: this.calculateCosts(this.state.simulator.getProcessedCommands(), this.state.simulator.getLayout()),
        });
      } catch (e) {
        this.setState({
          errorMessage: e.message,
        });
      }
    }
  };

  // convert the layout from the core layout to the UI layout
  processLayout(simulatorLayout: BlockWithType[][], bulldozerBlock: Block): BlockType[][] {
    const uiLayout = simulatorLayout.map((r) => r.map((b) => b.landType as BlockType));
    uiLayout[bulldozerBlock.row][bulldozerBlock.col] = "b";

    return uiLayout;
  }

  calculateCosts(commands: ProcessedCommand[], site: BlockWithType[][]): CostingEngine.CostingResponse {
    return CostingEngine.calculateCost({ commands: this.transformCommands(commands), site: site });
  }

  // convert the commands from the simulator format to the costing module format
  transformCommands(commands: ProcessedCommand[]): CostingEngine.CommandWithTypes[] {
    return commands.map((c) => ({ command: c.command, landTypes: c.response.processedBlocks.map((b) => b.landType) }));
  }
}

export default App;
