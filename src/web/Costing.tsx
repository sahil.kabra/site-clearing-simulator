import { CostingResponse } from "../engine/core/Costing";
import "./Costing.css";

export type CostingProps = CostingResponse;

export function Costing(props: CostingProps) {
  return (
    <div data-testid="costing" className="costing">
      <table className="costing-table">
        <thead>
          <tr>
            <th>Description</th>
            <th>Quantity</th>
            <th>Cost</th>
          </tr>
        </thead>
        <tbody>
          <CostingItem description="Communication overhead" count={props.commands.count} cost={props.commands.cost} />
          <CostingItem description="Fuel" count={props.fuel.count} cost={props.fuel.cost} />
          <CostingItem description="Legal costs" count={props.protectedTree.count} cost={props.protectedTree.cost} />
          <CostingItem description="Paint repairs" count={props.repairs.count} cost={props.repairs.cost} />
          <CostingItem
            description="Uncleared squares"
            count={props.unclearedSquares.count}
            cost={props.unclearedSquares.cost}
          />
          <CostingTotal description="Total" cost={calculateTotal(props)} />
        </tbody>
      </table>
      <div className="input-commands">
        <h2 className="input-commands-header">Commands entered</h2>
        <span className="input-commands-command">{props.commands.input.join(", ")}</span>
      </div>
    </div>
  );
}

function CostingItem(props: { description: string; count: number; cost: number }) {
  return (
    <tr>
      <td className="description">
        <label>{props.description}</label>
      </td>
      <td className="quantity">
        <span>{props.count}</span>
      </td>
      <td className="cost">
        <span>{props.cost}</span>
      </td>
    </tr>
  );
}
function CostingTotal(props: { description: string; cost: number }) {
  return (
    <tr className="costing-total">
      <td className="description">
        <label>{props.description}</label>
      </td>
      <td className="cost" colSpan={2}>
        <span>{props.cost}</span>
      </td>
    </tr>
  );
}

function calculateTotal(costs: CostingProps): number {
  return Object.values(costs).reduce((pv, cv) => pv + cv.cost, 0);
}
