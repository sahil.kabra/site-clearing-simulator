import "./Aside.css";

export function Aside() {
  return (
    <div className="aside">
      <Legend />
      <Commands />
    </div>
  );
}

function Legend() {
  return (
    <div>
      <h2>Legend</h2>
      <table className="legend-table">
        <tbody>
          <tr>
            <td>{"\u276f"}</td>
            <td>Bulldozer facing east</td>
          </tr>
          <tr>
            <td>o</td>
            <td>Plain land</td>
          </tr>
          <tr>
            <td>r</td>
            <td>Rocky land</td>
          </tr>
          <tr>
            <td>t</td>
            <td>Tree</td>
          </tr>
          <tr>
            <td>T</td>
            <td>Protected tree. Should NOT be cleared</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

function Commands() {
  return (
    <div>
      <h2>Commands</h2>
      <table className="command-table">
        <tbody>
          <tr>
            <td>a[n]</td>
            <td>Advance the bulldozer. Optionally, specify number of blocks</td>
          </tr>
          <tr>
            <td>l</td>
            <td>turn the bulldozer left</td>
          </tr>
          <tr>
            <td>r</td>
            <td>turn the bulldozer right</td>
          </tr>
          <tr>
            <td>q</td>
            <td>Quit</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
