import { Direction, LandType } from "../engine/core/Site";
import "./Block.css";

export type BlockType = LandType | "b";

export type BlockProps = {
  type: BlockType;
  bulldozerDirection: Direction;
};

export function Block(props: BlockProps) {
  const className = getClassName(props.type, props.bulldozerDirection);
  return (
    <div data-testid="block" className={"block " + className}>
      <label>{DisplayRepr[props.type]}</label>
    </div>
  );
}

function getClassName(type: BlockType, direction: Direction): string {
  switch (type) {
    case LandType.Clear:
      return "block-clear";
    case LandType.Plain:
      return "block-plain";
    case LandType.Tree:
      return "block-tree";
    case LandType.Rocky:
      return "block-rocky";
    case LandType.ProtectedTree:
      return "block-protected-tree";
    case "b":
      return "block-bulldozer " + direction;
  }
}

const Arrow = "\u276f";

const DisplayRepr = {
  [LandType.Clear]: "",
  [LandType.Plain]: "o",
  [LandType.Rocky]: "r",
  [LandType.Tree]: "t",
  [LandType.ProtectedTree]: "T",
  b: Arrow,
};
