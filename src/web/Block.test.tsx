import { render, screen } from "@testing-library/react";
import { Direction, LandType } from "../engine/core/Site";
import { Block } from "./Block";

describe("Block component", () => {
  it.each`
    type                      | displayRepr | expectedClassName
    ${LandType.Clear}         | ${""}       | ${"block-clear"}
    ${LandType.Plain}         | ${"o"}       | ${"block-plain"}
    ${LandType.Tree}          | ${"t"}      | ${"block-tree"}
    ${LandType.Rocky}         | ${"r"}      | ${"block-rocky"}
    ${LandType.ProtectedTree} | ${"T"}      | ${"block-protected-tree"}
    ${"b"}                    | ${"\u276f"} | ${"block-bulldozer"}
  `("should render div with class name $expectedClassName and type $type", ({ type, expectedClassName, displayRepr }) => {
    render(<Block bulldozerDirection={Direction.East} type={type} />);

    expect(screen.getByTestId("block")).toHaveClass(expectedClassName);
    expect(screen.getByTestId("block")).toHaveTextContent(displayRepr);
  });
});
