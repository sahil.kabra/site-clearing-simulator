import { render, screen } from "@testing-library/react";
import { Direction } from "../engine/core/Site";
import { BlockType } from "./Block";
import { Site } from "./Site";

describe("Site component", () => {
  const layout = [[..."bot"],  [..."ort"], [..."oTT"]] as BlockType[][];

  it("should render the same number of rows as the layout", () => {
    render(<Site bulldozerDirection={Direction.East} layout={layout}/>);

    expect(screen.getAllByTestId("site-row")).toHaveLength(layout.length);
  });
});
