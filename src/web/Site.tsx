import { Direction } from "../engine/core/Site";
import { Block, BlockType } from "./Block";
import "./Site.css";

export type SiteProps = {
  layout: BlockType[][];
  bulldozerDirection: Direction;
};

export function Site(props: SiteProps) {
  return (
    <div data-testid="site" className="site">
      {props.layout.map((row, index) => (
        <Row bulldozerDirection={props.bulldozerDirection} row={row} key={index}/>
      ))}
    </div>
  );
}

function Row(props: { row: BlockType[], bulldozerDirection: Direction }) {
  return (
    <div data-testid="site-row" className="row">
      {props.row.map((b, index) => (
        <Block bulldozerDirection={props.bulldozerDirection} type={b} key={index}/>
      ))}
    </div>
  );
}
