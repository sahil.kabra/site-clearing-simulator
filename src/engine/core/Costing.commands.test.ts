import { Command } from "./Command";
import { calculateCost, CommandWithTypes } from "./Costing";
import { createSite, LandType } from "./Site";

describe("costing.commands", () => {
  it("should return a cost of 1 for one command", () => {
    const commands = [{ command: Command.Left(), landTypes: [] }];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.commands.cost).toBe(1);
    expect(cost.commands.count).toBe(1);
    expect(cost.commands.input).toMatchObject(["left"]);
  });
  it("should not count include the quit for cost calculation", () => {
    const commands = [{ command: Command.Quit(), landTypes: [] }];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.commands.cost).toBe(0);
    expect(cost.commands.count).toBe(0);
    expect(cost.commands.input).toMatchObject(["quit"]);
  });

  it("should return total cost of all commands entered", () => {
    const commands = [
      { command: Command.Left(), landTypes: [] },
      { command: Command.Advance(), landTypes: [LandType.Plain] },
      { command: Command.Right(), landTypes: [] },
      { command: Command.Quit(), landTypes: [] },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.commands.cost).toBe(3);
    expect(cost.commands.count).toBe(3);
    expect(cost.commands.input).toMatchObject(["left", "advance", "right", "quit"]);
  });
  it("should return description for all the commands", () => {
    const commands = [
      { command: Command.Left(), landTypes: [] },
      { command: Command.Advance(4), landTypes: [LandType.Plain] },
      { command: Command.Right(), landTypes: [] },
      { command: Command.Quit(), landTypes: [] },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.commands.cost).toBe(3);
    expect(cost.commands.count).toBe(3);
    expect(cost.commands.input).toMatchObject(["left", "advance 4", "right", "quit"]);
  });

  describe("no commands entered", () => {
    const commands: CommandWithTypes[] = [];

    it("should return no fuel and commands cost", () => {
      const cost = calculateCost({
        commands,
        site: createSite(["tortto", "tttttr"]),
      });

      expect(cost.fuel.cost).toBe(0);
      expect(cost.fuel.count).toBe(0);

      expect(cost.commands.cost).toBe(0);
      expect(cost.commands.count).toBe(0);
      expect(cost.commands.input).toHaveLength(0);
    });
    it("should return cost for all site uncleared", () => {
      const cost = calculateCost({
        commands,
        site: createSite(["torroo", "oooooo", "oooToo", "ottoor"]),
      });

      expect(cost.unclearedSquares.count).toBe(23);
      expect(cost.unclearedSquares.cost).toBe(69);
      expect(cost.commands.input).toHaveLength(0);
    });
  });
});
