import { Command, getCommandProcessor } from "./Command";
import { createSite, Direction, LandType } from "./Site";

describe("commands", () => {
  it.each`
    command | expectedDirection | expectedBlock | simulationEnded
    ${"l"}  | ${"n"}            | ${"0,-1"}     | ${false}
    ${"r"}  | ${"s"}            | ${"0,-1"}     | ${false}
    ${"q"}  | ${"e"}            | ${"0,-1"}     | ${true}
  `(
    "should process the $command command",
    ({ command, expectedDirection, expectedBlock, simulationEnded }) => {
      const [row, col] = expectedBlock
        .split(",")
        .map((v: string) => parseInt(v, 10));

      const response = getCommandProcessor(Command.fromShortForm(command)!).process({
        block: { row: row, col: col },
        direction: Direction.East,
        site: createSite(["oooo", "oooo"]),
      });

      expect(response.direction).toBe(expectedDirection);
      expect(response.processedBlocks).toHaveLength(0);
      expect(response.endSimulation).toEqual(simulationEnded);
    }
  );
  it.each`
    command
    ${"abc"}
    ${"1ll"}
  `("should not return a value for invalid command", ({ command }) => {
    expect(Command.fromShortForm(command)).toBeFalsy();
  });

  describe("advance command", () => {
    it("should advance the bulldozer by one block from the start position", () => {
      const response = getCommandProcessor(Command.Advance()).process({
        block: { row: 0, col: -1 },
        direction: Direction.East,
        site: createSite(["oooooo", "ttottt"]),
      });

      expect(response.processedBlocks).toHaveLength(1);
      expect(response.processedBlocks[0]).toMatchObject({
        row: 0,
        col: 0,
        landType: LandType.Plain,
      });
    });
    it("should advance the bulldozer by the number of times in the command", () => {
      const response = getCommandProcessor(Command.Advance(4)).process({
        block: { row: 0, col: -1 },
        direction: Direction.East,
        site: createSite(["oooooo", "ttottt"]),
      });

      expect(response.processedBlocks).toHaveLength(4);
      expect(response.processedBlocks[3]).toMatchObject({
        row: 0,
        col: 3,
        landType: LandType.Plain,
      });
    });
    it("should clear any block that it moves on", () => {
      const response = getCommandProcessor(Command.Advance(4)).process({
        block: { row: 0, col: -1 },
        direction: Direction.East,
        site: createSite(["otrooo", "ttottt"]),
      });

      expect(response.site[0][1].landType).toBe(LandType.Clear);
      expect(response.site[0][2].landType).toBe(LandType.Clear);
    });
    it("should not end the simulation for a valid move", () => {
      const response = getCommandProcessor(Command.Advance(6)).process({
        block: { row: 0, col: -1 },
        direction: Direction.East,
        site: createSite(["oooooo", "ttottt"]),
      });

      expect(response.endSimulation).toBe(false);
    });
  });

  describe("exit conditions", () => {
    it("should exit if the command moves beyond the boundary", () => {
      const response = getCommandProcessor(Command.Advance(7)).process({
        block: { row: 0, col: -1 },
        direction: Direction.East,
        site: createSite(["otrooo", "ttottt"]),
      });

      expect(response.endSimulation).toBe(true);
    });
    it("should exit if there is a preservable tree in the area", () => {
      const response = getCommandProcessor(Command.Advance(7)).process({
        block: { row: 0, col: -1 },
        direction: Direction.East,
        site: createSite(["oTrooo", "ttottt"]),
      });

      expect(response.endSimulation).toBe(true);
    });
    it("should not clear a preservable tree", () => {
      const response = getCommandProcessor(Command.Advance(7)).process({
        block: { row: 0, col: -1 },
        direction: Direction.East,
        site: createSite(["oTrooo", "ttottt"]),
      });

      expect(response.site[0][1].landType).toBe(LandType.ProtectedTree);
    });
  });
});
