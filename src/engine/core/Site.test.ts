import * as Site from "./Site";

describe("A site", () => {
  describe("site create layout from string", () => {
    test("should return plain land type when it is plain", () => {
      const site = Site.createSite(["tottt", "trrto"]);

      expect(Site.getLandType(site, { row: 0, col: 1 })).toBe(Site.LandType.Plain);
    });
    test("should return rocky land type when it is rocky", () => {
      const site = Site.createSite(["tortto", "tttttr"]);

      expect(Site.getLandType(site, { row: 0, col: 2 })).toBe(Site.LandType.Rocky);
    });
    test("should return tree land type when it is a tree", () => {
      const site = Site.createSite(["tortto", "tttttr"]);

      expect(Site.getLandType(site, { row: 0, col: 0 })).toBe(Site.LandType.Tree);
    });
    test("should return preserved tree land type when it is a preserved tree", () => {
      const site = Site.createSite(["tortto", "tttttT"]);

      expect(Site.getLandType(site, { row: 1, col: 5 })).toBe(Site.LandType.ProtectedTree);
    });
    test("should return null when the block is out of bounds", () => {
      const site = Site.createSite(["tortto", "tttttT"]);

      expect(Site.getLandType(site, { row: 3, col: 5 })).toBeNull();
    });
    test("should throw error when the land type is invalid", () => {
      expect(() => Site.createSite(["tostto", "tttttT"])).toThrowError("invalid land type s");
    });
  });

  describe("get blocks", () => {
    it("should return 1 block by default", () => {
      const site = Site.createSite(["tortto", "tttttT"]);

      const blocks = Site.getBlocks(site, { row: 0, col: 2 }, Site.Direction.East);

      expect(blocks).toHaveLength(1);
      expect(blocks[0]).toMatchObject({
        row: 0,
        col: 3,
        landType: Site.LandType.Tree,
      });
    });
    it("should return `numBlocks` when all of them are within bounds", () => {
      const site = Site.createSite(["tortto", "tttttT"]);

      const blocks = Site.getBlocks(site, { row: 0, col: 0 }, Site.Direction.East, 5);

      expect(blocks).toHaveLength(5);
      expect(blocks[0]).toMatchObject({
        row: 0,
        col: 1,
        landType: Site.LandType.Plain,
      });
      expect(blocks[1]).toMatchObject({
        row: 0,
        col: 2,
        landType: Site.LandType.Rocky,
      });
      expect(blocks[2]).toMatchObject({
        row: 0,
        col: 3,
        landType: Site.LandType.Tree,
      });
      expect(blocks[3]).toMatchObject({
        row: 0,
        col: 4,
        landType: Site.LandType.Tree,
      });
      expect(blocks[4]).toMatchObject({
        row: 0,
        col: 5,
        landType: Site.LandType.Plain,
      });
    });
    it("should return less than `numBlocks` when it exceeds bounds", () => {
      const site = Site.createSite(["tortto", "tttttT"]);

      const blocks = Site.getBlocks(site, { row: 0, col: 0 }, Site.Direction.East, 15);

      expect(blocks).toHaveLength(5);
      expect(blocks[0]).toMatchObject({
        row: 0,
        col: 1,
        landType: Site.LandType.Plain,
      });
      expect(blocks[1]).toMatchObject({
        row: 0,
        col: 2,
        landType: Site.LandType.Rocky,
      });
      expect(blocks[2]).toMatchObject({
        row: 0,
        col: 3,
        landType: Site.LandType.Tree,
      });
      expect(blocks[3]).toMatchObject({
        row: 0,
        col: 4,
        landType: Site.LandType.Tree,
      });
      expect(blocks[4]).toMatchObject({
        row: 0,
        col: 5,
        landType: Site.LandType.Plain,
      });
    });
    it("should return empty array when numBlocks is 0", () => {
      const site = Site.createSite(["tortto", "tttttT"]);

      const blocks = Site.getBlocks(site, { row: 0, col: 2 }, Site.Direction.East, 0);

      expect(blocks).toHaveLength(0);
    });
    it.each`
      layout                           | startBlock | direction | numBlocks | endBlock
      ${"oootro,ottTrr,toooor,tttrro"} | ${"3,2"}   | ${"n"}    | ${3}      | ${"0,2,o"}
      ${"oootro,ottTrr,toooor,tttrro"} | ${"0,0"}   | ${"e"}    | ${2}      | ${"0,2,o"}
      ${"oootro,ottTrr,toooor,tttrro"} | ${"0,4"}   | ${"s"}    | ${1}      | ${"1,4,r"}
      ${"oootro,ottTrr,toooor,tttrro"} | ${"2,3"}   | ${"w"}    | ${3}      | ${"2,0,t"}
    `(
      "should return $numBlocks blocks in the $direction direction",
      ({ layout, startBlock, direction, numBlocks, endBlock }) => {
        const site = Site.createSite(layout.split(","));
        const [row, col] = startBlock.split(",").map((v: string) => parseInt(v, 10));

        const blocks = Site.getBlocks(site, { row: row, col: col }, direction as Site.Direction, parseInt(numBlocks));

        expect(blocks).toHaveLength(parseInt(numBlocks));

        const b = blocks[blocks.length - 1];
        expect(`${b.row},${b.col},${b.landType}`).toBe(endBlock);
      },
    );
  });
  it("should clear the block", () => {
    const block = { row: 0, col: 0 };
    const site = Site.createSite(["tortto", "tttttT"]);

    const clearedSite = Site.clearBlock(site, block);

    expect(clearedSite[block.row][block.col].landType).toBe(Site.LandType.Clear);
    expect(site[block.row][block.col].landType).toBe(Site.LandType.Tree);
  });
});
