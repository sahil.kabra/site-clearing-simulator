import { Command } from "./Command";
import { calculateCost } from "./Costing";
import { LandType } from "./Site";

describe("costing.protectTree", () => {
  it("should return the total cost of destroying a protected tree", () => {
    const commands = [
      { command: Command.Left(), landTypes: [] },
      { command: Command.Advance(), landTypes: [LandType.ProtectedTree] },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.protectedTree.cost).toBe(10);
    expect(cost.protectedTree.count).toBe(1);
  });
  it("should return 0 when no protected trees have been destroyed", () => {
    const commands = [
      { command: Command.Left(), landTypes: [] },
      {
        command: Command.Advance(),
        landTypes: [LandType.Rocky, LandType.Tree, LandType.Plain],
      },
      { command: Command.Quit(), landTypes: [] },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.protectedTree.cost).toBe(0);
    expect(cost.protectedTree.count).toBe(0);
  });
});
