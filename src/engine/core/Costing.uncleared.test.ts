import { calculateCost } from "./Costing";
import { createSite } from "./Site";

describe("costing.unclearedSquares", () => {
  it("should return the cost of one uncleared squares", () => {
    const cost = calculateCost({ commands: [], site: createSite(["o"]) });

    expect(cost.unclearedSquares.cost).toBe(3);
    expect(cost.unclearedSquares.count).toBe(1);
  });
  it("should return the total cost of uncleared squares", () => {
    const cost = calculateCost({ commands: [], site: createSite(["cccc", "oroo", "otto", "otro"]) });

    expect(cost.unclearedSquares.count).toBe(12);
    expect(cost.unclearedSquares.cost).toBe(36);
  });
  it("should not return a protected tree as uncleared", () => {
    const cost = calculateCost({ commands: [], site: createSite(["cccc", "ccTcc"]) });

    expect(cost.unclearedSquares.count).toBe(0);
    expect(cost.unclearedSquares.cost).toBe(0);
  });
  it("should return 0 when all squares have been cleared", () => {
    const cost = calculateCost({ commands: [], site: createSite(["ccc", "ccc"]) });

    expect(cost.unclearedSquares.cost).toBe(0);
    expect(cost.unclearedSquares.count).toBe(0);
  });
});
