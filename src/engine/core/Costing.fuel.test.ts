import { Command } from "./Command";
import { calculateCost } from "./Costing";
import { LandType } from "./Site";

describe("costing.fuel", () => {
  it.each`
    land              | expectedCost | expectedCount
    ${LandType.Clear} | ${1}         | ${1}
    ${LandType.Plain} | ${1}         | ${1}
    ${LandType.Rocky} | ${2}         | ${2}
    ${LandType.Tree}  | ${2}         | ${2}
  `("should return the correct cost of fuel for land $land", ({ land, expectedCost, expectedCount }) => {
    const commands = [{ command: Command.Advance(), landTypes: [land] }];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.fuel.cost).toBe(expectedCost);
    expect(cost.fuel.count).toBe(expectedCount);
  });
  it("should sum the cost of fuel for multiple land types", () => {
    const commands = [
      {
        command: Command.Advance(),
        landTypes: [LandType.Clear, LandType.Rocky, LandType.Tree, LandType.Rocky, LandType.Plain],
      },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.fuel.cost).toBe(8);
    expect(cost.fuel.count).toBe(8);
  });
  it("should not include preservable tree to count fuel cost", () => {
    const commands = [
      {
        command: Command.Advance(),
        landTypes: [LandType.Plain, LandType.ProtectedTree],
      },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.fuel.cost).toBe(1);
    expect(cost.fuel.count).toBe(1);
  });
  it("should return no fuel for no land cleared", () => {
    const commands = [{ command: Command.Left(), landTypes: [] }];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.fuel.cost).toBe(0);
    expect(cost.fuel.count).toBe(0);
  });
});
