import { Command, CommandsEnum } from "./Command";
import { BlockWithType, LandType } from "./Site";

const landTypeToFuel = {
  [LandType.Clear]: 1,
  [LandType.Plain]: 1,
  [LandType.Rocky]: 2,
  [LandType.Tree]: 2,
  [LandType.ProtectedTree]: 2, // not used but need to add for ts
};

enum CostItems {
  Command = 0,
  Fuel,
  ProtectedTree,
  Repair,
  Uncleared,
}

const costs = {
  [CostItems.Command]: 1,
  [CostItems.Fuel]: 1,
  [CostItems.ProtectedTree]: 10,
  [CostItems.Repair]: 2,
  [CostItems.Uncleared]: 3,
};

export type CommandWithTypes = {
  command: Command;
  landTypes: LandType[];
};

export type CostingArgs = {
  commands: CommandWithTypes[];
  site: BlockWithType[][];
};

export type CostingResponse = {
  commands: { count: number; cost: number; input: string[] };
  fuel: { count: number; cost: number };
  protectedTree: { count: number; cost: number };
  repairs: { count: number; cost: number };
  unclearedSquares: { count: number; cost: number };
};

export function calculateCost({ commands, site }: CostingArgs): CostingResponse {
  const commandCount = getTotalCommands(commands);
  const inputCommands = getInputCommands(commands);
  const fuelUsage = getFuelUsage(commands);
  const treesDestroyed = getProtectTreesDestroyed(commands);
  const unclearedSquares = getUnclearedSquares(site);
  const paintDamage = getPaintDamage(commands);
  return {
    commands: { count: commandCount, cost: commandCount * costs[CostItems.Command], input: inputCommands },
    fuel: { count: fuelUsage, cost: fuelUsage * costs[CostItems.Fuel] },
    protectedTree: { count: treesDestroyed, cost: treesDestroyed * costs[CostItems.ProtectedTree] },
    repairs: { count: paintDamage, cost: paintDamage * costs[CostItems.Repair] },
    unclearedSquares: { count: unclearedSquares, cost: unclearedSquares * costs[CostItems.Uncleared] },
  };
}

function getInputCommands(commands: CommandWithTypes[]): string[] {
  return commands.map((c) => `${c.command.value}${c.command.count > 1 ? " " + c.command.count : ""}`);
}

function getProtectTreesDestroyed(commands: CommandWithTypes[]): number {
  return commands
    .flatMap((c) => c.landTypes)
    .filter((l) => l === LandType.ProtectedTree).length;
}

function getPaintDamage(commands: CommandWithTypes[]): number {
  return commands
    .flatMap((c) => c.landTypes.slice(0, -1)) // remove the last element of the array since stopping on a tree will not damage paint
    .filter((l) => l === LandType.Tree).length; // count the damage
}

function getFuelUsage(commands: CommandWithTypes[]): number {
  return commands
    .flatMap((c) => c.landTypes)
    .filter((l) => l !== LandType.ProtectedTree)
    .reduce((fuel, l) => fuel + landTypeToFuel[l], 0);
}

function getTotalCommands(commands: CommandWithTypes[]): number {
  return commands.filter((c) => c.command.value !== CommandsEnum.Quit).length;
}

function getUnclearedSquares(site: BlockWithType[][]): number {
  return site
    .flatMap((r) => r.map((b) => b.landType))
    .filter((l) => [LandType.Tree, LandType.Plain, LandType.Rocky].includes(l)).length;
}
