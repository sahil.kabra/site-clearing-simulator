export enum LandType {
  Clear = "c",
  Plain = "o",
  ProtectedTree = "T",
  Rocky = "r",
  Tree = "t",
}

export type Block = {
  row: number;
  col: number;
};

export type BlockWithType = Block & { landType: LandType };

export enum Direction {
  North = "n",
  East = "e",
  South = "s",
  West = "w",
}

export function createSite(siteMap: string[]): BlockWithType[][] {
  return siteMap.map((row, index) =>
    [...row].map((t, col) => ({ row: index, col: col, landType: parse(t) } as BlockWithType)),
  );
}

export function getLandType(site: BlockWithType[][], block: Block): LandType | null {
  if (withinBounds(site)(block)) {
    return site[block.row][block.col].landType;
  }
  return null;
}

/**
 * Returns `numBlocks` blocks with the block starting after `fromBlock` in the
 * `direction` specfied.
 * `fromBlock` will not be included in the returned array
 */
export function getBlocks(
  site: BlockWithType[][],
  fromBlock: Block,
  direction: Direction,
  numBlocks: number = 1,
): BlockWithType[] {
  if (numBlocks < 1) return [];

  return (
    [...Array(numBlocks).keys()]
      // get the next block in the correct direction
      .reduce(
        (acc: Block[]) => {
          acc.push(getNextBlock(acc[acc.length - 1], direction));
          return acc;
        },
        [fromBlock],
      )
      // remove the starting block
      .slice(1)
      // filter for only those blocks that are within bounds
      .filter(withinBounds(site))
      // return the landtype for blocks within the boundary
      .map((block: Block) => cloneObj(site[block.row][block.col]))
  );
}

export function clearBlock(layout: BlockWithType[][], block: Block): BlockWithType[][] {
  const l = layout.map((r) => r.map(cloneObj));
  l[block.row][block.col].landType = LandType.Clear;
  return l;
}

function incrementRow(value: number, direction: Direction): number {
  if (direction === Direction.North) return value - 1;
  if (direction === Direction.South) return value + 1;
  return value;
}

function incrementCol(value: number, direction: Direction): number {
  if (direction === Direction.West) return value - 1;
  if (direction === Direction.East) return value + 1;
  return value;
}

// shallow clone an object
function cloneObj<T>(o: T): T {
  return { ...o } as T;
}

function withinBounds(layout: Block[][]) {
  return (block: Block) => block.row < layout.length && block.col < layout[0].length;
}

function getNextBlock(fromBlock: Block, direction: Direction): Block {
  return {
    row: incrementRow(fromBlock.row, direction),
    col: incrementCol(fromBlock.col, direction),
  };
}

function parse(landType: string): LandType {
  if (!Object.values(LandType).includes(landType as LandType)) throw new Error(`invalid land type ${landType}`);

  return landType as LandType;
}
