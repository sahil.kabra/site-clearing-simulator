import { Block, BlockWithType, clearBlock, Direction, getBlocks, LandType } from "./Site";

export enum CommandsEnum {
  Advance = "advance",
  Left = "left",
  Right = "right",
  Quit = "quit",
}

export type ProcessedCommand = {
  command: Command;
  response: CommandProcessorResponse;
};

export type CommandProcessorArgs = {
  block: Block;
  direction: Direction;
  site: BlockWithType[][];
};

export type CommandProcessorResponse = {
  processedBlocks: BlockWithType[];
  direction: Direction;
  endSimulation: boolean;
  site: BlockWithType[][];
};

export type CommandProcessor = {
  process: (args: CommandProcessorArgs) => CommandProcessorResponse;
};

export class Command {
  static Advance = (count: number = 1) => new Command(CommandsEnum.Advance, "a", count);
  static Left = () => new Command(CommandsEnum.Left, "l", 1);
  static Right = () => new Command(CommandsEnum.Right, "r", 1);
  static Quit = () => new Command(CommandsEnum.Quit, "q", 1);

  readonly value: CommandsEnum;
  readonly shortValue: string;
  readonly count: number;

  private constructor(value: CommandsEnum, shortValue: string, count: number = 1) {
    this.value = value;
    this.shortValue = shortValue;
    this.count = count;
  }

  static fromShortForm(shortValue: string, count: number = 1): Command | undefined {
    return [Command.Advance(count), Command.Left(), Command.Right(), Command.Quit()].find(
      (c) => c.shortValue === shortValue,
    );
  }
}

export function getCommandProcessor(command: Command): CommandProcessor {
  switch (command.value) {
    case CommandsEnum.Left:
    case CommandsEnum.Right:
      return { process: turn({ value: command.value }) };
    case CommandsEnum.Advance:
      return { process: advance(command) };
    case CommandsEnum.Quit:
    default:
      return { process: quit(command) };
  }
}

export function getProcessedLandTypes(processedCommands: ProcessedCommand[]): LandType[] {
  return processedCommands.flatMap((c) => c.response.processedBlocks).map((b) => b?.landType);
}

function quit(_: Command) {
  return (args: CommandProcessorArgs): CommandProcessorResponse => ({
    processedBlocks: [],
    direction: args.direction,
    endSimulation: true,
    site: args.site,
  });
}

function turn(command: { value: CommandsEnum.Left | CommandsEnum.Right }) {
  return (args: CommandProcessorArgs): CommandProcessorResponse => ({
    processedBlocks: [],
    direction: directionConfig[args.direction][command.value] as Direction,
    endSimulation: false,
    site: args.site,
  });
}

function advance(command: Command) {
  return (args: CommandProcessorArgs): CommandProcessorResponse => {
    const { blocks, treeFound } = stopAtPreservableTree(
      getBlocks(args.site, args.block, args.direction, command.count),
    );

    const clearedSite = blocks
      .filter((b) => LandType.ProtectedTree !== b.landType) // Do not mark a protected tree as cleared
      .reduce((site, block) => clearBlock(site, block), args.site);

    return {
      site: clearedSite,
      direction: args.direction,
      endSimulation: treeFound || blocks.length < command.count,
      processedBlocks: blocks,
    };
  };
}

function stopAtPreservableTree(blocks: BlockWithType[]): {
  blocks: BlockWithType[];
  treeFound: boolean;
} {
  const index = blocks.findIndex((b) => b.landType === LandType.ProtectedTree);

  if (index === -1) return { treeFound: false, blocks: blocks };
  else return { treeFound: true, blocks: blocks.slice(0, index + 1) };
}

const directionConfig = {
  [Direction.North]: {
    [CommandsEnum.Left]: Direction.West,
    [CommandsEnum.Right]: Direction.East,
  },
  [Direction.East]: {
    [CommandsEnum.Left]: Direction.North,
    [CommandsEnum.Right]: Direction.South,
  },
  [Direction.South]: {
    [CommandsEnum.Left]: Direction.East,
    [CommandsEnum.Right]: Direction.West,
  },
  [Direction.West]: {
    [CommandsEnum.Left]: Direction.South,
    [CommandsEnum.Right]: Direction.North,
  },
};
