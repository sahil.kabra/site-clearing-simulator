import { Command } from "./Command";
import { calculateCost } from "./Costing";
import { LandType } from "./Site";

describe("costing.repairs", () => {
  it("should return a cost for repairs when bulldozer passes a single block with tree", () => {
    const commands = [
      { command: Command.Left(), landTypes: [] },
      {
        command: Command.Advance(),
        landTypes: [
          LandType.Plain,
          LandType.Rocky,
          LandType.Tree,
          LandType.Plain,
        ],
      },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.repairs.cost).toBe(2);
    expect(cost.repairs.count).toBe(1);
  });
  it("should return total cost of repairs when bulldozer passed multiple blocks with trees", () => {
    const commands = [
      { command: Command.Left(), landTypes: [] },
      {
        command: Command.Advance(),
        landTypes: [
          LandType.Plain,
          LandType.Rocky,
          LandType.Tree,
          LandType.Plain,
        ],
      },
      { command: Command.Left(), landTypes: [] },
      { command: Command.Right(), landTypes: [] },
      {
        command: Command.Advance(),
        landTypes: [
          LandType.Tree,
          LandType.Rocky,
          LandType.Tree,
          LandType.Plain,
        ],
      },
      { command: Command.Advance(), landTypes: [LandType.Tree] },
      { command: Command.Quit(), landTypes: [] },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.repairs.cost).toBe(6);
    expect(cost.repairs.count).toBe(3);
  });
  it("should not include any trees that bulldozer stopped on", () => {
    const commands = [
      { command: Command.Left(), landTypes: [] },
      {
        command: Command.Advance(),
        landTypes: [LandType.Plain, LandType.Rocky, LandType.Tree],
      },
      { command: Command.Left(), landTypes: [] },
      { command: Command.Right(), landTypes: [] },
      {
        command: Command.Advance(),
        landTypes: [LandType.Plain, LandType.Rocky, LandType.Tree],
      },
      { command: Command.Advance(), landTypes: [LandType.Tree] },
      { command: Command.Quit(), landTypes: [] },
    ];

    const cost = calculateCost({ commands, site: [] });

    expect(cost.repairs.cost).toBe(0);
    expect(cost.repairs.count).toBe(0);
  });
});
