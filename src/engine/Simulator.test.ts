import { getProcessedLandTypes } from "./core/Command";
import { LandType } from "./core/Site";
import { Simulator } from "./Simulator";

describe("Simulator", () => {
  it.each`
    command | expectedDirection | expectedBlock | simulationEnded
    ${"l"}  | ${"n"}            | ${"0,-1"}     | ${false}
    ${"r"}  | ${"s"}            | ${"0,-1"}     | ${false}
    ${"q"}  | ${"e"}            | ${"0,-1"}     | ${true}
  `(
    "should process the $command command",
    ({ command, expectedDirection, expectedBlock, simulationEnded }) => {
      const [row, col] = expectedBlock
        .split(",")
        .map((v: string) => parseInt(v, 10));
      const simulator = new Simulator(["ooo", "tto"]);

      simulator.processCommand(command);

      expect(simulator.getBulldozerDirection()).toEqual(expectedDirection);
      expect(simulator.getBulldozerBlock()).toEqual({ row: row, col: col });
      expect(simulator.getProcessedCommands()).toHaveLength(1);

      const processedCommand = simulator.getProcessedCommands()[0];
      expect(processedCommand.command.shortValue).toEqual(command);
      expect(processedCommand.response.processedBlocks).toHaveLength(0);
      expect(processedCommand.response.direction).toBe(expectedDirection);
      expect(processedCommand.response.endSimulation).toEqual(simulationEnded);
    }
  );

  it.each`
    command
    ${"abc"}
    ${"1ll"}
  `("should throw error for command $command", ({ command }) => {
    const simulator = new Simulator(["ooo", "tto"]);

    expect(() => simulator.processCommand(command)).toThrowError(
      `invalid command ${command}`
    );
  });

  describe("advance command", () => {
    it("should advance the bulldozer to clear land", () => {
      const simulator = new Simulator(["oooooo", "ttottt"]);

      simulator.processCommand("a4");

      expect(simulator.getBulldozerBlock()).toMatchObject({
        row: 0,
        col: 3,
        landType: LandType.Plain,
      });
    });
    it("should advance the bulldozer by one block", () => {
      const simulator = new Simulator(["oooooo", "ttottt"]);

      debugger;
      simulator.processCommand("a");

      expect(simulator.getBulldozerBlock()).toMatchObject({
        row: 0,
        col: 0,
        landType: LandType.Plain,
      });
    });
    it("should return the type of land being cleared", () => {
      const simulator = new Simulator(["oooooo", "ttottt"]);

      simulator.processCommand("a");

      expect(simulator.getBulldozerBlock()).toMatchObject({
        row: 0,
        col: 0,
        landType: LandType.Plain,
      });
    });
    it("should be able to run multiple commands", () => {
      const simulator = new Simulator(["oooooo", "ttottt"]);

      simulator.processCommand("a");
      simulator.processCommand("r");
      simulator.processCommand("a");
      simulator.processCommand("l");
      simulator.processCommand("a3");

      expect(simulator.getBulldozerBlock()).toMatchObject({
        row: 1,
        col: 3,
        landType: LandType.Tree,
      });
      expect(getLandTypes(simulator)).toEqual(
        "ottot"
      );
    });
    it("should clear an already visited site", () => {
      const simulator = new Simulator(["otrooo", "ttottt"]);

      simulator.processCommand("a4"); // otro
      simulator.processCommand("l");
      simulator.processCommand("l");

      simulator.processCommand("a3"); // ccc

      expect(getLandTypes(simulator)).toEqual(
        "otroccc"
      );
    });

    describe("exit conditions", () => {
      it("should exit if the command moves beyond the boundary", () => {
        const simulator = new Simulator(["otrooo", "ttottt"]);

        simulator.processCommand("a7");

        expect(simulator.isEnded()).toBe(true);
      });
      it("should exit if there is a preservable tree in the area", () => {
        const simulator = new Simulator(["oTrooo", "ttottt"]);

        simulator.processCommand("a7");

        expect(simulator.isEnded()).toBe(true);
      });
      it("should not clear a preservable tree", () => {
        const simulator = new Simulator(["oTrooo", "ttottt"]);

        simulator.processCommand("a7");

        expect(getLandTypes(simulator)).toEqual(
          "oT"
        );
      });
    });
  });
});

function getLandTypes(simulator: Simulator): string {
  return getProcessedLandTypes(simulator.getProcessedCommands()).join("");
}
