import { Command, CommandProcessorResponse, getCommandProcessor, ProcessedCommand } from "./core/Command";
import * as Site from "./core/Site";

export type Bulldozer = {
  block: Site.Block;
  direction: Site.Direction;
};

type SimulatorState = {
  bulldozer: Bulldozer;
  processedCommands: ProcessedCommand[];
  simulationEnded: boolean;
  site: Site.BlockWithType[][];
};

export class Simulator {
  private state: SimulatorState;

  constructor(layout: string[]) {
    this.state = {
      bulldozer: {
        block: { row: 0, col: -1 },
        direction: Site.Direction.East,
      },
      processedCommands: [],
      simulationEnded: false,
      site: Site.createSite(layout),
    };
  }

  processCommand(command: string): void {
    if (command.length > 2) throw new Error(`invalid command ${command}`);

    const commandToProcess = this.parseCommand(command);

    if (!commandToProcess) {
      throw new Error(`invalid command ${command}`);
    }

    const response = getCommandProcessor(commandToProcess).process({
      block: this.state.bulldozer.block,
      direction: this.state.bulldozer.direction,
      site: this.state.site,
    });

    this.updateState(commandToProcess, response);
  }

  getBulldozerDirection(): Site.Direction {
    return this.state.bulldozer.direction;
  }

  getBulldozerBlock(): Site.Block {
    return this.state.bulldozer.block;
  }

  getProcessedCommands(): ProcessedCommand[] {
    return this.state.processedCommands;
  }

  isEnded(): boolean {
    return this.state.simulationEnded;
  }

  getLayout(): Site.BlockWithType[][] {
    return this.state.site;
  }

  private parseCommand(command: string): Command | undefined {
    if (/\d$/.test(command)) {
      return Command.fromShortForm(command.charAt(0), parseInt(command.charAt(1), 10));
    } else {
      return Command.fromShortForm(command);
    }
  }

  private updateState(command: Command, response: CommandProcessorResponse) {
    const bulldozerBlock =
      response.processedBlocks.length !== 0
        ? response.processedBlocks.slice(response.processedBlocks.length - 1)[0]
        : this.state.bulldozer.block;

    this.state = {
      bulldozer: {
        block: bulldozerBlock,
        direction: response.direction,
      },
      processedCommands: [...this.state.processedCommands, { command, response }],
      simulationEnded: response.endSimulation,
      site: response.site,
    };
  }
}
