import { render, screen } from '@testing-library/react';
import App from './App';

test('shoud mount app successfully', () => {
  render(<App />);
  const element = screen.getByTestId("fileInput");
  expect(element).toBeInTheDocument();
});
