# Site clearing simulation

## Description

This project implements a simple app for simulating the site clearing operation.
In the app, the user has to upload a text file that has the site layout.

For eg:
~~~
ooootooo
ooorrroo
ToooTooo
~~~

where:
- `o`: denotes plain land
- `t`: denotes tree that needs to be cleared
- `T`: denotes a protected tree
- `r`: denotes rocky land that needs to be cleared

The bulldozer will always enter the site from the top left block and face east.
When the simulation ends an itemised expense report will be displayed.

### Input file format
This is a [sample file](sample.txt)
- The input file should only contain the valid characters describing the layout.
- All lines should be of the same length.
- The top left block should not have a protected tree (`T`).

### Commands
Commands are executed once the user enters the command and presses enter. Please enter the commands one at a time in the
provided input box.

Valid commands are:
- `a`: Advance the bulldozer. A number may be entered after the letter to specify the number of blocks to move, eg: `a4`
- `l`: Turn the bulldozer left.
- `r`: Turn the bulldozer right.
- `q`: Quit the simulation.

### Ending simulation
The simulation will end when:
 - The user enters the `q` command.
 - The user attempts to move beyond the site boundary.
 - The user attempts to clear a protected tree (`T`).

## Installation and Running

### Prerequisites
* yarn
* node 14.16

### Scripts
* `yarn install --frozen-lockfile`
This will install all the dependencies needed to run and develop on the project.

* `yarn start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

* `yarn test`
Runs the tests

* `yarn build`
Builds the app for production use to the `build` folder.

## Solution Approach
### Design
- The source code has been split into a `web` and an `engine` directory.
- The `web` directory contains React components.
- The `engine` directory contains typescript files that have the logic of the simulator.

#### Web
- The `App` component contains the state of the simulator.
- It interacts with various functional components to update the UI as per the command entered.
- It also interacts with `engine.Simulator` and `engine.core.Costing` to process commands and display costs to the user.
- The components are divided as:
    * `Site` - displays the entire site.
    * `Block` - displays an individual block in the `Site`.
    * `Aside` - displays the legend and the commands help section.
    * `Costing` - displays the costing information once the simulation has ended.

#### Engine
- The `engine.Simulator` class plays the role of an orchestrator. It contains all mutable operations and decides how the
  data flow into the system. This is also the place where the state of the simulator is stored.
- The `engine.core` package contains the core of the simulation engine. It has the logic organized as pure functions.
- `engine.core.Commands` module contains functions that process commands that are received from the user.
- `engine.core.Site` module handles operations related to the site.
- `engine.core.Costing` module has the logic to calculate costs based on the input supplied.


# References

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Possible improvements
- The implementation does not have enough tests for the UI. This needs to be improved.
- Look at using Immutable js for the collections and other data stored in the application.
- The UI is very basic and the layout has not been tested for a large site.
- Has only been tested on chrome.
- Costing using a table which is not responsive.
